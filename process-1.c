#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int pid;
    int mypipefd[2];
    int ret,i,k;
    char pipeadress[10];
    pipe(mypipefd);
    char ch[128]="";
    char ch2[128]="";
    char buf[128]="";
    int chint[16];
    int bufint[16];
    int xorlist[16];
    char newch[16][4];
    read(atoi(argv[0]),ch,128);
    strcpy(buf,argv[1]);
    /*printf("\n(proc 1) :%s\n ",ch);*/
    /*printf("\n(proc key 1) :%s\n ",argv[1]);*/

        char * pchb;

        pchb = strtok (buf,"-");
    int j=0;
    while (pchb != NULL)
    {
        bufint[j]=atoi(pchb);
        pchb = strtok (NULL, "-");
        j++;

    }


    char * pch;


    pch = strtok (ch,"-");

    i=0;
    while (pch != NULL)
    {

        chint[i]=atoi(pch);
        pch = strtok (NULL, "-");
        i++;
    }

    for(k=0;k<16;k++){

        xorlist[k]=chint[k] ^ bufint[k];
        /*printf("xor---%d\n",xorlist[k]);*/
    }

     for(k=0;k<16;k++){
        sprintf(newch[k],"%d",xorlist[k]);
        /*printf("--string : %s\n",newch[k]);*/
        if(k==15)
            strcat(ch2,newch[k]);
        else{
            strcat(ch2,newch[k]);
            strcat(ch2,"-");
        }
    }
        strcat(ch2,"\n");
        /*printf("--string : %s\n",ch2);*/
        FILE *dosya;
        dosya = fopen("process-1.txt","a");
        fprintf(dosya,"%s",ch2);
        fclose(dosya);

    
    pid = fork();
    if(pid>0) {
        /*parent process*/
        /*printf("parent process-1\n");*/
        write(mypipefd[1],ch2,128);


    }
    else if (pid==0){
        /* chield process */
        /*printf("Chield porecess-1\n");*/


        sprintf(pipeadress,"%d",mypipefd[0]);

        execlp("./process-2",pipeadress,argv[0],NULL);
        perror("failed:\n");

    }
    wait(NULL);
    /*printf("bb process-1 \n");*/


    return 0;
}
